<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="script.js" ></script>
<link rel="stylesheet" type="text/css" href="style.css">

<body>
	<ul>
		<li><a href="./index.php">Home</a></li>
		<li><a href="./projects.php">Projects</a></li>
		<!--<li><a href="https://gitlab.com/bryanthinton01">Git</a></li>-->
		<li><a href="./CS260">CS 260</a></li>
		<li><a href="./about.php">About</a></li>
	</ul><br>
	<div class='parent' style="padding-left: 0px; padding-right: 0px;">
		<img src="./img/cover.jpg" width="100.05%" style="margin-left: -.3px;">
		<p style="padding-left: 30px; padding-right: 30px;">
			My name is Bryant Hinton, and I am currently a Junior in BYU’s Computer Science program. During which time I have learned the basics of good programming, and built many programs in languages such as C++ and Java.<br><br>

			For the past several years, I have been working at Brigham Young University’s department of Risk Management doing computer support, and more recently, web programming. So, I am familiar with many of the technologies in use today, and adept at using them to build and maintain web pages. For example, I used a combination of HTML, CSS, JavaScript, PHP, and jQuery to build several forms and webpages at risk.byu.edu, as well as updated older code at the same site. I also have experience with bash and terminal, am familiar with how to install and run apache server, and have experience pulling from and interacting with SQL databases.
		</p>
		<div style="width: 46%; display: inline-block; padding-left: 30px; padding-right: 30px; vertical-align: top;">
			I am a hard worker, and am always striving to improve my skills and to do some good in the world. I am a quick learner, and also love to learn new things. I work well with others, and I believe I have the skills and work ethic to make a difference for good.
		</div>
		<div style="width: 46%; display: inline-block; padding-right: 30px; vertical-align: top;">
			<img src="./img/family.jpg" width="100%">
		</div>
	</div>
</body>

<a href='http://www.freepik.com/free-vector/grey-linen-texture-background_835399.htm' style="font-size: .6em; float: right; color: black;">Background by Freepik</a>