<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="script.js" ></script>
<link rel="stylesheet" type="text/css" href="style.css">

<body>
	<ul>
		<li><a href="./index.php">Home</a></li>
		<li><a href="./projects.php">Projects</a></li>
		<!--<li><a href="https://gitlab.com/bryanthinton01">Git</a></li>-->
		<li><a href="./CS260">CS 260</a></li>
		<li><a href="./about.php">About</a></li>
	</ul><br>
	<div class='parent'>
		<h4 style="text-align: center;"> This is a non-exhaustive list of the pages I've worked on while at BYU Risk Management</h4>
	    <a class="button box" href="https://risk.byu.edu/forms/drill"><p class="box2">Drill Form</p></a>
	    <a class="button box" href="https://risk.byu.edu/forms/driverincident"><p class="box2">Driver's Incident Form</p></a>
		<a class="button box" href="https://risk.byu.edu/forms/generalinjury"><p class="box2">General Injury Form</p></a>
		<a class="button box" href="https://risk.byu.edu/forms/safetyreport"><p class="box2">General Incident Form</p></a>
		<a class="button box" href="https://risk.byu.edu/forms/eventapproval"><p class="box2">Event Approval Form</p></a>
		<a class="button box" href="https://risk.byu.edu/forms/youthinjury"><p class="box2">EFY Injury Form</p></a>
		<a class="button box" href="https://risk.byu.edu/forms/pickupissue"><p class="box2">Pickup Issue Form</p></a>
		<a class="button box" href="https://risk.byu.edu/insurance/events.php"><p class="box3">Event Approval Process Flowchart</p></a>
		<a class="button box" href="https://risk.byu.edu/insurance/eventsFAQ.php"><p class="box2">Event Approval FAQ</p></a>
		<a class="button box" href="https://risk.byu.edu/insurance/vendorlist.php"><p class="box3">Approved Off-Campus Vendor List (Maintained)</p></a>
		<a class="button box" href="https://policy.byu.edu"><p class="box3">Policy (Made code PHP 7 compatible)</p></a>
		
	</div>
</body>

<a href='http://www.freepik.com/free-vector/grey-linen-texture-background_835399.htm' style="font-size: .6em; float: right; color: black;">Background by Freepik</a>
